package poloniex

import (
	"crypto/hmac"
	"crypto/sha512"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/url"
	"strconv"
	"time"

	"github.com/franela/goreq"
)

type (
	Base struct {
		Error    string
		Success  int64
		Response string
	}

	Balances map[string]Balance
	Balance  struct {
		Available float64 `json:",string"`
		OnOrders  float64 `json:"onOrders,string"`
		BTCValue  float64 `json:"btcValue,string"`
	}

	AccountBalancesTemp struct {
		Exchange map[string]string
		Margin   map[string]string
		Lending  map[string]string
	}

	AccountBalances struct {
		Exchange map[string]float64
		Margin   map[string]float64
		Lending  map[string]float64
	}

	Addresses map[string]string

	DepositsWithdrawals struct {
		Deposits    []Deposit
		Withdrawals []Withdrawal
	}
	Deposit struct {
		Currency      string
		Address       string
		Amount        float64 `json:",string"`
		Confirmations int64
		TXID          string `json:"txid"`
		Timestamp     int64
		Status        string
	}
	Withdrawal struct {
		WithdrawalNumber int64 `json:"withdrawalNumber"`
		Currency         string
		Address          string
		Amount           float64 `json:",string"`
		Timestamp        int64
		Status           string
	}

	OpenOrders []OpenOrder
	OpenOrder  struct {
		OrderNumber string
		Type        string
		Rate        float64 `json:",string"`
		Amount      float64 `json:",string"`
		Total       float64 `json:",string"`
	}
	OpenOrdersAll map[string]OpenOrders

	PrivateTradeHistory      []PrivateTradeHistoryEntry
	PrivateTradeHistoryEntry struct {
		Date        string
		Rate        float64 `json:",string"`
		Amount      float64 `json:",string"`
		Total       float64 `json:",string"`
		OrderNumber string
		Type        string
	}
	PrivateTradeHistoryAll map[string]PrivateTradeHistory

	Buy struct {
		OrderNumber     int64
		ResultingTrades []ResultingTrade
	}
	ResultingTrade struct {
		Amount  float64 `json:",string"`
		Rate    float64 `json:",string"`
		Date    string
		Total   float64 `json:",string"`
		TradeID string  `json:"tradeID"`
		Type    string
	}
	Sell struct {
		Buy
	}

	MoveOrder struct {
		Base
		OrderNumber     int64 `json:",string"`
		ResultingTrades []ResultingTrade
	}

	LoanOffer struct {
		Base
		OrderID int64 `json:"orderID"`
	}

	OpenLoanOffers map[string][]OpenLoanOffer
	OpenLoanOffer  struct {
		ID        int64   `json:"id"`
		Rate      float64 `json:",string"`
		Amount    float64 `json:",string"`
		Duration  int64
		Renewable bool
		AutoRenew int64 `json:"autoRenew"`
		Date      string
		DateTaken time.Time
	}

	ActiveLoans struct {
		Provided []ActiveLoan
	}
	ActiveLoan struct {
		ID        int64 `json:"id"`
		Currency  string
		Rate      float64 `json:",string"`
		Amount    float64 `json:",string"`
		Range     int64
		Renewable bool
		AutoRenew int64 `json:"autoRenew"`
		Date      string
		DateTaken time.Time
		Fees      float64 `json:",string"`
	}
)

func (p *Poloniex) Balances() (balances Balances, err error) {
	p.private("returnCompleteBalances", nil, &balances)
	return
}

func (p *Poloniex) AccountBalances() (balances AccountBalances, err error) {
	b := AccountBalancesTemp{}
	p.private("returnAvailableAccountBalances", nil, &b)
	balances = AccountBalances{Exchange: map[string]float64{}, Margin: map[string]float64{}, Lending: map[string]float64{}}
	for k, v := range b.Exchange {
		balances.Exchange[k], _ = strconv.ParseFloat(v, 64)
	}
	for k, v := range b.Margin {
		balances.Margin[k], _ = strconv.ParseFloat(v, 64)
	}
	for k, v := range b.Lending {
		balances.Lending[k], _ = strconv.ParseFloat(v, 64)
	}
	return
}

func (p *Poloniex) Addresses() (addresses Addresses, err error) {
	p.private("returnDepositAddresses", nil, &addresses)
	return
}

func (p *Poloniex) GenerateNewAddress(currency string) (address string, err error) {
	params := url.Values{}
	params.Add("currency", currency)
	b := Base{}
	err = p.private("generateNewAddress", params, &b)
	address = b.Response
	return
}

func (p *Poloniex) DepositsWithdrawals() (depositsWithdrawals DepositsWithdrawals, err error) {
	params := url.Values{}
	params.Add("start", fmt.Sprintf("%d", time.Now().Add(-5208*time.Hour).Unix()))
	params.Add("end", "9999999999")
	err = p.private("returnDepositsWithdrawals", params, &depositsWithdrawals)
	return
}

func (p *Poloniex) OpenOrders(pair string) (openOrders OpenOrders, err error) {
	params := url.Values{}
	params.Add("currencyPair", pair)
	err = p.private("returnOpenOrders", params, &openOrders)
	return
}

func (p *Poloniex) OpenOrdersAll() (openOrders OpenOrdersAll, err error) {
	params := url.Values{}
	params.Add("currencyPair", "all")
	err = p.private("returnOpenOrders", params, &openOrders)
	return
}

func (p *Poloniex) PrivateTradeHistory(pair string) (history PrivateTradeHistory, err error) {
	params := url.Values{}
	params.Add("currencyPair", pair)
	err = p.private("returnTradeHistory", params, &history)
	return
}

func (p *Poloniex) PrivateTradeHistoryAll() (history PrivateTradeHistoryAll, err error) {
	params := url.Values{}
	params.Add("currencyPair", "all")
	err = p.private("returnTradeHistory", params, &history)
	return
}

func (p *Poloniex) CancelOrder(orderNumber int64) (success bool, err error) {
	params := url.Values{}
	params.Add("orderNumber", fmt.Sprintf("%d", orderNumber))
	b := Base{}
	err = p.private("cancelOrder", params, &b)
	success = b.Success == 1
	return
}

func (p *Poloniex) Buy(pair string, rate, amount float64) (buy Buy, err error) {
	params := url.Values{}
	params.Add("currencyPair", pair)
	params.Add("rate", fmt.Sprintf("%.8f", rate))
	params.Add("amount", fmt.Sprintf("%.8f", amount))
	err = p.private("buy", params, &buy)
	return
}

func (p *Poloniex) Sell(pair string, rate, amount float64) (sell Sell, err error) {
	params := url.Values{}
	params.Add("currencyPair", pair)
	params.Add("rate", fmt.Sprintf("%.8f", rate))
	params.Add("amount", fmt.Sprintf("%.8f", amount))
	err = p.private("sell", params, &sell)
	return
}

func (p *Poloniex) MoveOrder(orderNumber int64, rate float64) (moveOrder MoveOrder, err error) {
	params := url.Values{}
	params.Add("orderNumber", fmt.Sprintf("%d", orderNumber))
	params.Add("rate", fmt.Sprintf("%.8f", rate))
	err = p.private("moveOrder", params, &moveOrder)
	return
}

func (p *Poloniex) LoanOffer(currency string, amount float64, duration int, renew bool, lendingRate float64) (loanOffer LoanOffer, err error) {
	params := url.Values{}
	params.Add("currency", currency)
	params.Add("amount", fmt.Sprintf("%.8f", amount))
	params.Add("lendingRate", fmt.Sprintf("%.8f", lendingRate/100.0))
	params.Add("duration", fmt.Sprintf("%d", duration))
	r := 0
	if renew {
		r = 1
	}
	params.Add("autoRenew", fmt.Sprintf("%d", r))
	err = p.private("createLoanOffer", params, &loanOffer)
	return
}

func (p *Poloniex) CancelLoanOffer(orderNumber int64) (success bool, err error) {
	params := url.Values{}
	params.Add("orderNumber", fmt.Sprintf("%d", orderNumber))
	b := Base{}
	err = p.private("cancelLoanOffer", params, &b)
	success = b.Success == 1
	return
}

func (p *Poloniex) OpenLoanOffers() (openLoanOffers OpenLoanOffers, err error) {
	err = p.private("returnOpenLoanOffers", nil, &openLoanOffers)
	return
}

func (p *Poloniex) ActiveLoans() (activeLoans ActiveLoans, err error) {
	err = p.private("returnActiveLoans", nil, &activeLoans)
	provided := activeLoans.Provided
	n := []ActiveLoan{}
	for k := range provided {
		v := provided[k]
		v.Renewable = v.AutoRenew == 1
		t, err := time.Parse("2006-01-02 15:04:05", v.Date)
		if err == nil {
			v.DateTaken = t
		}
		n = append(n, v)
	}
	activeLoans.Provided = n
	return
}

func (p *Poloniex) ToggleAutoRenew(orderNumber int64) (success bool, err error) {
	params := url.Values{}
	params.Add("orderNumber", fmt.Sprintf("%d", orderNumber))
	b := Base{}
	err = p.private("toggleAutoRenew", params, &b)
	success = b.Success == 1
	return
}

// make a call to the jsonrpc api, marshal into v
func (p *Poloniex) private(method string, params url.Values, v interface{}) error {
	if params == nil {
		params = url.Values{}
	}
	params.Set("nonce", p.GetNonce())
	params.Set("command", method)
	postData := params.Encode()

	req := goreq.Request{Method: "POST", Uri: PRIVATEURI, Body: postData, ContentType: "application/x-www-form-urlencoded", Accept: "application/json", Timeout: 10 * time.Second}

	req.AddHeader("Sign", p.sign(postData))
	req.AddHeader("Key", p.Key)
	req.AddHeader("Content-Length", strconv.Itoa(len(postData)))

	res, err := req.Do()
	if err != nil {
		return err
	}

	defer res.Body.Close()

	s, err := res.Body.ToString()
	if err != nil {
		return err
	}

	if p.debug {
		fmt.Println(s)
	}
	base := Base{}
	err = json.Unmarshal([]byte(s), &base)
	if base.Error != "" {
		return fmt.Errorf("API ERROR: %s", base.Error)
	}
	err = json.Unmarshal([]byte(s), v)
	if err != nil {
		return err
	}

	return nil
}

// generate hmac-sha512 hash, hex encoded
func (c *Poloniex) sign(payload string) string {
	mac := hmac.New(sha512.New, []byte(c.Secret))
	mac.Write([]byte(payload))
	return hex.EncodeToString(mac.Sum(nil))
}
